const path = require('path')
const resolve = require('rollup-plugin-node-resolve')
const commonjs = require('rollup-plugin-commonjs')
const babel = require('rollup-plugin-babel')
const json = require('rollup-plugin-json')
const vue = require('rollup-plugin-vue')
const postcss = require('rollup-plugin-postcss')

const inputPath = path.resolve(__dirname, './src/index.js')
const outputUmdPath = path.resolve(__dirname, './dist/fulink.datav.js')
const outputEsPath = path.resolve(__dirname, './dist/fulink.datav.es.js')

module.exports = {
    input: inputPath,
    output: [
        {
            file: outputUmdPath,
            format: 'umd',
            name: 'fulinkDatav',
            globals: {
                vue: 'Vue'
            }
        },
        {
            file: outputEsPath,
            format: 'es',
            globals: {
                vue: 'Vue'
            }
        }
    ],
    plugins: [//有顺序
        vue(),
        babel({
            exclude: 'node_modules/**',
            runtimeHelpers: true,
            plugins: [
                ['@babel/transform-runtime', {
                    regenerator: true
                }]
            ]
        }),
        resolve(), //解决第三方依赖打包问题
        commonjs(), // commonjs 语法模块
        json(), //正确实现json 文件打包
        //打包vue 组件
        postcss({
            plugins: []
        })
    ],
    external: ["vue","echarts"]
}
