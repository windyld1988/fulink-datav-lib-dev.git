## 目录

* [安装](#install)
* [配置](#config)
* 组件
    * [全屏容器](#fulink-container)
    * [Loading加载](#fulink-loading)
    * [基础滑动列表](#base-scroll-list)
    * [数字翻牌器](#count-to)
    * [图表](#vue-echarts)
   


<h3 id="install">安装</h3>

```
npm i -S fulink-datav-libs echarts
npm i -D sass@1.26.5 sass-loader@8.0.2 
```

<h3 id="config">配置</h3>

`main.js`

```javascript
import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import datavComponent from 'fulink-datav-libs'

createApp(App)
    .use(router)
    .use(datavComponent)
    .mount('#app')

```

<h3 id="fulink-container">全屏容器</h3>

>注意：
>
>当大屏电脑比例一致，电脑屏幕分辨率为1920*1080时，设计稿为电脑分辨率尺寸大小；
>
>分辨率为3840 * 2160时设计稿优先级是3840 * 2160、1920 * 1080、之间同比例数值，当小于电脑分辨率时开发方式要适应屏幕大小。


```javascript
   <fulink-container :options="{width: 3840, height: 2160}">
      content
   </fulink-container>
```


<h3 id="fulink-loading">Loading加载</h3>


```javascript
   <fulink-loading width="70" height="70" v-if="loading">
      <div class="loading-text">数据大屏加载中...</div>
   </fulink-loading>
```

#### 效果图

![](https://media.giphy.com/media/Prtpdg8slPZSJpkPIF/giphy.gif)


<h3 id="base-scroll-list">基础滑动列表</h3>

```javascript
 <base-scroll-list :config = "config" / >
```

**e.g.**

```
const salesListMockData = [{"order":"北京 -10%","shop":"北京 -19%","rider":"北京 -12%","newShop":"北京 -17%","avgOrder":"北京 -8%"},{"order":"上海 +19%","shop":"上海 -7%","rider":"上海 +6%","newShop":"上海 +7%","avgOrder":"上海 +21%"},{"order":"广州 -6%","shop":"广州 -5%","rider":"广州 +23%","newShop":"广州 -22%","avgOrder":"广州 +12%"},{"order":"深圳 -19%","shop":"深圳 -14%","rider":"深圳 -13%","newShop":"深圳 +7%","avgOrder":"深圳 -7%"},{"order":"南京 -22%","shop":"南京 -7%","rider":"南京 -7%","newShop":"南京 +16%","avgOrder":"南京 -8%"},{"order":"杭州 +15%","shop":"杭州 +9%","rider":"杭州 -10%","newShop":"杭州 -11%","avgOrder":"杭州 +7%"},{"order":"合肥 -8%","shop":"合肥 -5%","rider":"合肥 +9%","newShop":"合肥 -7%","avgOrder":"合肥 -12%"},{"order":"济南 +20%","shop":"济南 +8%","rider":"济南 +16%","newShop":"济南 +3%","avgOrder":"济南 -12%"},{"order":"太原 +8%","shop":"太原 -4%","rider":"太原 +5%","newShop":"太原 +10%","avgOrder":"太原 +25%"},{"order":"成都 -7%","shop":"成都 +19%","rider":"成都 -24%","newShop":"成都 +13%","avgOrder":"成都 -3%"},{"order":"重庆 +4%","shop":"重庆 -24%","rider":"重庆 +12%","newShop":"重庆 +9%","avgOrder":"重庆 +4%"},{"order":"苏州 +16%","shop":"苏州 -8%","rider":"苏州 +19%","newShop":"苏州 -17%","avgOrder":"苏州 -15%"},{"order":"无锡 +15%","shop":"无锡 +12%","rider":"无锡 +20%","newShop":"无锡 -13%","avgOrder":"无锡 -20%"},{"order":"常州 -18%","shop":"常州 -19%","rider":"常州 +15%","newShop":"常州 +5%","avgOrder":"常州 +8%"},{"order":"温州 -21%","shop":"温州 +20%","rider":"温州 +8%","newShop":"温州 -21%","avgOrder":"温州 +11%"},{"order":"哈尔滨 -19%","shop":"哈尔滨 -17%","rider":"哈尔滨 -9%","newShop":"哈尔滨 -23%","avgOrder":"哈尔滨 +18%"},{"order":"长春 -2%","shop":"长春 +18%","rider":"长春 -20%","newShop":"长春 -4%","avgOrder":"长春 -24%"},{"order":"大连 +22%","shop":"大连 -15%","rider":"大连 -6%","newShop":"大连 -16%","avgOrder":"大连 +9%"},{"order":"沈阳 -15%","shop":"沈阳 -8%","rider":"沈阳 -17%","newShop":"沈阳 +14%","avgOrder":"沈阳 -14%"},{"order":"拉萨 -4%","shop":"拉萨 -17%","rider":"拉萨 -17%","newShop":"拉萨 +19%","avgOrder":"拉萨 -21%"},{"order":"呼和浩特 -10%","shop":"呼和浩特 +15%","rider":"呼和浩特 +17%","newShop":"呼和浩特 +21%","avgOrder":"呼和浩特 +11%"},{"order":"武汉 +15%","shop":"武汉 -12%","rider":"武汉 +18%","newShop":"武汉 +15%","avgOrder":"武汉 -7%"},{"order":"南宁 -17%","shop":"南宁 -13%","rider":"南宁 -23%","newShop":"南宁 -13%","avgOrder":"南宁 -14%"}]
```

```javascript
const config = ref({})
const data = []
const headerIndexData = []
const aligns = []

for (let i = 0; i < props.data.length; i++) {
    data[i] = []
    if (i % 2 === 0) {
        headerIndexData[i] = `<div style="width:100%;height:100%;display:flex;align-items:center;justify-content:center;background: rgb(40,40,40)">
            <div style="width:15px;height:15px;background:rgb(72,122,72);border-radius:50%;border:1px solid #fff;"/>
          </div>`
    } else {
        headerIndexData[i] = `<div style="width:100%;height:100%;display:flex;align-items:center;justify-content:center;background: rgb(40,40,40)">
            <div style="width:15px;height:15px;background:rgb(38,88,104);border-radius:50%;border:1px solid #fff;"/>
          </div>`
    }

    for (let j = 0; j < 5; j++) {
        aligns.push('center')
        let text = ''
        switch (j) {
            case 0: // 第一列数据
                text = props.data[i].order
                break
            case 1: // 第二列数据
                text = props.data[i].shop
                break
            case 2: // 第三列数据
                text = props.data[i].rider
                break
            case 3: // 第四列数据
                text = props.data[i].newShop
                break
            case 4: // 第五列数据
                text = props.data[i].avgOrder
                break
            default:
                break
        }
        if (j === 1 || j === 3) {
            data[i].push(`<div style="color: rgb(178,209,126)">${text}</div>`)
        } else {
            data[i].push(`<div>${text}</div>`)
        }
    }
}

config.value = {
    headerData: ['城市订单量', '店铺数', '接单骑手人数', '新店铺数量', '人均订单量'],
    headerBg: 'rgb(80,80,80)',
    headerHeight: 55,
    headerFontSize: 24,
    headerIndex: true,
    headerIndexContent: '',
    headerIndexData,
    rowNum: 10,
    rowBg: ['rgb(40,40,40)', 'rgb(55,55,55)'],
    rowColor: '#FFF',
    rowFontSize: 24,
    data,
    aligns
}

return {
    config
}
```

#### 效果图

![](https://media.giphy.com/media/FQJDoL4BzreYviSMJQ/giphy.gif)

#### API
| 属性             | 说明                                   | 默认值 |
| :--------       | :--------                              | :--: |
| headerData      | 列表头部数据                             | `[ ]` |
| data            | 列表内容数据(二维数组格式)                 | `[ ]`  |
| headerStyle     | 列表头部样式                             | `[ ]`  |
| rowStyle        | 行样式                                  |  `[ ]` |
| rowBg           | 行背景色(数组[0]对应奇数行，数组[1]对应偶数行)| `[ ]`  |
| aligns          | 居中方式                                 | `[ ]`  |
| headerColor     | 列表头部字体颜色                          | `#FFF` |
| rowColor        | 列表内容字体颜色                          | `#000`  |
| headerFontSize  | 列表头部字段大小                          | `28`   |
| rowFontSize     | 列表内容字体大小                          |  `28`   |
| headerBg        | 列表头部背景色                     |`rgb(90,90,90)` |      
| headerHeight    | 列表头部高度                              |  `35`|
| headerIndex     | 列表是否展示序号                           | `false` |
| headerIndexData | 序号列数据内容                            | `[html]`|
| rowNum          | 列表展示几行      						| `10`  |


<h3 id="count-to">数字翻牌器</h3>

```javascript
 <count-to :startVal='startVal' :endVal='endVal' :duration='3000' />
```


**e.g.**

```javascript
<template>
  <div class="count-to">
    <count-to
      :startVal='startVal'
      :endVal='endVal'
      :duration='3000'
    />
  </div>
</template>

<script>
export default {
  data () {
    return {
      startVal: 0,
      endVal: 2021
    }
  }
}
</script>

<style lang="scss" scoped>
.count-to{
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 42px;
  font-weight: 700;
}
</style>
```

#### 效果图
![](https://media.giphy.com/media/tgt4spAEKbmUpJlWer/giphy.gif)


#### API
| 属性       |    说明                  | 默认值|
| :-------- | :--------                | :--  |
| startVal  | 开始值 Number             |  0   |
| endVal    |结束值 Number              | 2017 |
| duration  |持续时间，以毫秒为单位	Number | 3000 |
| autoplay  |自动播放	Boolean        | true |
| decimals  |要显示的小数位数	Number     | 0    |
| decimal   |十进制分割 String           | .    |
| prefix    |前缀    String             |  ''  |
| suffix    |后缀	String             | ''   |
| useEasing |使用缓和功能	Boolean        | true |
| easingFn  |缓和回调	Function       | —   |


>注意：当autoplay：true时，它将在startVal或endVal更改时自动启动**


#### 功能
| Function Name   | Description |
| :--------       | :--------    |
| mountedCallback | 挂载以后返回回调|
| start           |开始计数        | 
| pause           |暂停计数        | 
| reset           |重置countTo    |



<h3 id="vue-echarts">图表</h3>

>注意：
>
>这里用的echarts版本是5.1.1；
>
>option里面配置参数可以根据实际需求配置不同类型的图表。




```javascript
 <vue-echarts :option="option"/>
```

**e.g.**

```javascript
<template>
  <vue-echarts :option="option"/>
</template>

<script>
import { ref } from 'vue'

export default {
  setup () {
    const option = ref({})

    option.value = {
      xAxis: {},
      yAxis: {},
      series: [
        {
          type: 'bar',
          data: [10, 15, 20, 25, 30]
        }
      ]
    }

    return {
      option
    }
  }
}
</script>

```
#### 效果图
![](https://tva1.sinaimg.cn/large/008i3skNly1gublddyksgj60jf0bi74h02.jpg)
