import Icon from './components/Icon/index'
import SvgAnimation from './components/SvgAnimation/index'
import FulinkLoading from './components/FulinkLoading/index'
import FulinkFlyBox from './components/FulinkFlyBox/index'
import Container from './components/Container/index'
import FulinkRCBLogo from './components/FulinkRCBLogo/index'
import VueCountTo from './components/VueCountTo/index'
import VueEcharts from './components/VueEcharts/index'
import BaseScrollList from './components/BaseScrollList/index'

export default function (Vue) {
    Vue.use(Icon)
    Vue.use(SvgAnimation)
    Vue.use(FulinkLoading)
    Vue.use(FulinkFlyBox)
    Vue.use(Container)
    Vue.use(FulinkRCBLogo)
    Vue.use(VueCountTo)
    Vue.use(VueEcharts)
    Vue.use(BaseScrollList)
}
