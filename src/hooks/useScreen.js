import {ref, onMounted} from 'vue'

const width = ref(0)
const height = ref(0)

export default function useScreen(id) {
    let dom
    onMounted(() => {
        dom = document.getElementById(id)
        width.value = dom.clientWidth
        height.value = dom.clientHeight
    })

    return {
        width,
        height
    }
}
